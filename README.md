A very simple nano/micro-like text editor inside Panda3d. Previously part of panda3d-livecode.

The purpose is to enable simple text editing inside Panda3D, not to create
an optimized all-round editor (though nice that would be).

It is still under construction. Certain features don't work as you'd expect.


To Install:
`pip install p3dit && p3dit`


Controls:
---------

- arrows/home/end/pgup/pgdown = move cursor
- shift+move = select
- ctrl+arrow  = move cursor per-word
- ctrl c/x/v  = copy/cut/paste
- ctrl-n 	  = new file
- ctrl-s 	  = save file
- ctrl-o      = open file
- ctrl-q      = close file
- ctrl-tab    = hide/show editor

Features:
---------
- Python code highlighting with pygments
- Folder browser with DirectFolderBrowser

Wishlist:
---------
- Python interpreter
- Terminal emulator through pyte
- Hot-reloading panda3d apps inside editor (like panda3d-livecode)
