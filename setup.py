from setuptools import setup


def readme():
    with open('README.md') as readme_file:
        return readme_file.read()

__version__ = ''
#pylint: disable=exec-used
setup(
    name='p3dit',
    version=0.001,
    description='Simple text editor in Panda3D',
    long_description=readme(),
    long_description_content_type='text/markdown',
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    keywords='panda3d gamedev',
    url='https://codeberg.org/entikan/p3dit',
    author='jan Entikan',
    license='MIT',
    packages=['p3dit'],
    include_package_data=True,
    install_requires=[
        'panda3d',
        'pygments',
        'DirectFolderBrowser'
    ],
    entry_points={
        'console_scripts':[
            'p3dit=p3dit.cli:main',
        ]
    }
)
